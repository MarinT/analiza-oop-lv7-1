﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        // stvaranje grafickih objekata drawing imenika
        Pen pen = new Pen(Color.Black, 2);
        Graphics pbA1, pbA2, pbA3, pbB1, pbB2, pbB3, pbC1, pbC2, pbC3;


        char[,] values = new char[3, 3];


        int gameCount = 1, turnCount = 0, numberOfWinsPlayerOne = 0, numberOfWinsPlayerTwo = 0, i, j;
        string playerOneName = "", playerTwoName = "";

        bool turn = true, winScenario = false, pbA1Clicked = false, pbA2Clicked = false, pbA3Clicked = false, pbB1Clicked = false, pbB2Clicked = false,
            pbB3Clicked = false, pbC1Clicked = false, pbC2Clicked = false, pbC3Clicked = false;


        public Form2(string playerOne, string playerTwo)
        {
            InitializeComponent();

            pbA1 = pictureBoxA1.CreateGraphics();
            pbA2 = pictureBoxA2.CreateGraphics();
            pbA3 = pictureBoxA3.CreateGraphics();
            pbB1 = pictureBoxB1.CreateGraphics();
            pbB2 = pictureBoxB2.CreateGraphics();
            pbB3 = pictureBoxB3.CreateGraphics();
            pbC1 = pictureBoxC1.CreateGraphics();
            pbC2 = pictureBoxC2.CreateGraphics();
            pbC3 = pictureBoxC3.CreateGraphics();

            //postavljaom labele koje ce nam govoriti koji igrac je koliko puta pobijedio

            labelPlayerOneName.Text = playerOne + " wins: ";
            labelPlayerOneNumberOfWins.Text = "0";

            labelPlayerTwoName.Text = playerTwo + " wins: ";
            labelPlayerTwoNumberOfWins.Text = "0";

            playerOneName = playerOne;
            playerTwoName = playerTwo;

            //ova if-else petlja alternira koji ce igrac igrati prvi, bilo bi ne fer ostaviti sistem u kojem uvijek jedan igrac prvi igra 
            //premda je krizic kruzic igra koja jako ovisi o redoslijedu igranja

            if (gameCount % 2 == 0)
                labelTurnName.Text = playerOneName;

            else
                labelTurnName.Text = playerTwoName;

        }


        private void pictureBoxA1_Click(object sender, EventArgs e)
        {
            // provjerava je li polje vec prije bilo iskoristeno
            if (pbA1Clicked == false)
            {
                if (turn)
                {
                    // crta x
                    pbA1.DrawLine(pen, 60, 60, 0, 0);
                    pbA1.DrawLine(pen, 60, 0, 0, 60);


                    values[0, 0] = 'X';

                }

                else
                {
                    // crta oks
                    pbA1.DrawEllipse(pen, 15, 15, 30, 30);

                    values[0, 0] = 'O';

                }

                turn = !turn; // oznacava promjenu iksa i oksa
                turnCount++; // javlja koliko je poteza vec odigrano
                pbA1Clicked = true; // javlja da je polje vec iskoristeno
                checkForWinner(labelTurnName.Text);

                // rotira imena igraca ciji je red igranja

                if (labelTurnName.Text == playerOneName)
                    labelTurnName.Text = playerTwoName;

                else
                    labelTurnName.Text = playerOneName;




            }

        }

        private void pictureBoxA2_Click(object sender, EventArgs e)
        {
            if (pbA2Clicked == false)
            {
                if (turn)
                {
                    pbA2.DrawLine(pen, 60, 60, 0, 0);
                    pbA2.DrawLine(pen, 60, 0, 0, 60);


                    values[0, 1] = 'X';

                }

                else
                {
                    pbA2.DrawEllipse(pen, 15, 15, 30, 30);

                    values[0, 1] = 'O';

                }

                turn = !turn;
                turnCount++;
                pbA2Clicked = true;
                checkForWinner(labelTurnName.Text);

                if (labelTurnName.Text == playerOneName)
                    labelTurnName.Text = playerTwoName;

                else
                    labelTurnName.Text = playerOneName;




            }

        }

        private void pictureBoxA3_Click(object sender, EventArgs e)
        {
            if (pbA3Clicked == false)
            {
                if (turn)
                {
                    pbA3.DrawLine(pen, 60, 60, 0, 0);
                    pbA3.DrawLine(pen, 60, 0, 0, 60);


                    values[0, 2] = 'X';

                }

                else
                {
                    pbA3.DrawEllipse(pen, 15, 15, 30, 30);

                    values[0, 2] = 'O';

                }

                turn = !turn;
                turnCount++;
                pbA3Clicked = true;
                checkForWinner(labelTurnName.Text);

                if (labelTurnName.Text == playerOneName)
                    labelTurnName.Text = playerTwoName;

                else
                    labelTurnName.Text = playerOneName;




            }

        }

        private void pictureBoxB1_Click(object sender, EventArgs e)
        {
            if (pbB1Clicked == false)
            {
                if (turn)
                {
                    pbB1.DrawLine(pen, 60, 60, 0, 0);
                    pbB1.DrawLine(pen, 60, 0, 0, 60);


                    values[1, 0] = 'X';

                }

                else
                {
                    pbB1.DrawEllipse(pen, 15, 15, 30, 30);

                    values[1, 0] = 'O';

                }

                turn = !turn;
                turnCount++;
                pbB1Clicked = true;
                checkForWinner(labelTurnName.Text);

                if (labelTurnName.Text == playerOneName)
                    labelTurnName.Text = playerTwoName;

                else
                    labelTurnName.Text = playerOneName;




            }

        }

        private void pictureBoxB2_Click(object sender, EventArgs e)
        {
            if (pbB2Clicked == false)
            {
                if (turn)
                {
                    pbB2.DrawLine(pen, 60, 60, 0, 0);
                    pbB2.DrawLine(pen, 60, 0, 0, 60);


                    values[1, 1] = 'X';

                }

                else
                {
                    pbB2.DrawEllipse(pen, 15, 15, 30, 30);

                    values[1, 1] = 'O';

                }

                turn = !turn;
                turnCount++;
                pbB2Clicked = true;
                checkForWinner(labelTurnName.Text);

                if (labelTurnName.Text == playerOneName)
                    labelTurnName.Text = playerTwoName;

                else
                    labelTurnName.Text = playerOneName;




            }

        }

        private void pictureBoxB3_Click(object sender, EventArgs e)
        {
            if (pbB3Clicked == false)
            {
                if (turn)
                {
                    pbB3.DrawLine(pen, 60, 60, 0, 0);
                    pbB3.DrawLine(pen, 60, 0, 0, 60);


                    values[1, 2] = 'X';

                }

                else
                {
                    pbB3.DrawEllipse(pen, 15, 15, 30, 30);

                    values[1, 2] = 'O';

                }

                turn = !turn;
                turnCount++;
                pbB3Clicked = true;
                checkForWinner(labelTurnName.Text);

                if (labelTurnName.Text == playerOneName)
                    labelTurnName.Text = playerTwoName;

                else
                    labelTurnName.Text = playerOneName;




            }

        }

        private void pictureBoxC1_Click(object sender, EventArgs e)
        {
            if (pbC1Clicked == false)
            {
                if (turn)
                {
                    pbC1.DrawLine(pen, 60, 60, 0, 0);
                    pbC1.DrawLine(pen, 60, 0, 0, 60);


                    values[2, 0] = 'X';

                }

                else
                {
                    pbC1.DrawEllipse(pen, 15, 15, 30, 30);

                    values[2, 0] = 'O';

                }

                turn = !turn;
                turnCount++;
                pbC1Clicked = true;
                checkForWinner(labelTurnName.Text);

                if (labelTurnName.Text == playerOneName)
                    labelTurnName.Text = playerTwoName;

                else
                    labelTurnName.Text = playerOneName;




            }

        }

        private void pictureBoxC2_Click(object sender, EventArgs e)
        {
            if (pbC2Clicked == false)
            {
                if (turn)
                {
                    pbC2.DrawLine(pen, 60, 60, 0, 0);
                    pbC2.DrawLine(pen, 60, 0, 0, 60);


                    values[2, 1] = 'X';

                }

                else
                {
                    pbC2.DrawEllipse(pen, 15, 15, 30, 30);

                    values[2, 2] = 'O';

                }

                turn = !turn;
                turnCount++;
                pbC2Clicked = true;
                checkForWinner(labelTurnName.Text);

                if (labelTurnName.Text == playerOneName)
                    labelTurnName.Text = playerTwoName;

                else
                    labelTurnName.Text = playerOneName;




            }

        }

        private void pictureBoxC3_Click(object sender, EventArgs e)
        {
            if (pbC3Clicked == false)
            {
                if (turn)
                {
                    pbC3.DrawLine(pen, 60, 60, 0, 0);
                    pbC3.DrawLine(pen, 60, 0, 0, 60);


                    values[2, 2] = 'X';

                }

                else
                {
                    pbC3.DrawEllipse(pen, 15, 15, 30, 30);

                    values[2, 2] = 'O';

                }

                turn = !turn;
                turnCount++;
                pbC3Clicked = true;
                checkForWinner(labelTurnName.Text);

                if (labelTurnName.Text == playerOneName)
                    labelTurnName.Text = playerTwoName;

                else
                    labelTurnName.Text = playerOneName;





            }
        }

        private void checkForWinner(string winner)
        {

            // horizntalna provjera

            if ((values[0, 0] == values[0, 1]) && (values[0, 1] == values[0, 2]) && (pbA1Clicked) && (pbA2Clicked) && (pbA3Clicked))
                winScenario = true;

            if ((values[1, 0] == values[1, 1]) && (values[1, 1] == values[1, 2]) && (pbB1Clicked) && (pbB2Clicked) && (pbB3Clicked))
                winScenario = true;

            if ((values[2, 0] == values[2, 1]) && (values[2, 1] == values[2, 2]) && (pbC1Clicked) && (pbC2Clicked) && (pbC3Clicked))
                winScenario = true;

            // vertikalno

            if ((values[0, 0] == values[1, 0]) && (values[1, 0] == values[2, 0]) && (pbA1Clicked) && (pbB1Clicked) && (pbC1Clicked))
                winScenario = true;

            if ((values[0, 1] == values[1, 1]) && (values[1, 1] == values[2, 1]) && (pbA2Clicked) && (pbB2Clicked) && (pbC2Clicked))
                winScenario = true;

            if ((values[0, 2] == values[1, 2]) && (values[1, 2] == values[2, 2]) && (pbA3Clicked) && (pbB3Clicked) && (pbC3Clicked))
                winScenario = true;


            // okomito

            if ((values[0, 0] == values[1, 1]) && (values[1, 1] == values[2, 2]) && (pbA1Clicked) && (pbB2Clicked) && (pbC3Clicked))
                winScenario = true;

            if ((values[0, 2] == values[1, 1]) && (values[1, 1] == values[2, 0]) && (pbA3Clicked) && (pbB2Clicked) && (pbC1Clicked))
                winScenario = true;

            //ukoliko postoji pobijednik
            if (winScenario == true)
            {

                //dodjeljuje partiju igracu broj jedan, ispisuje rezultat i pobijednika

                if (winner == playerOneName)
                {
                    ++numberOfWinsPlayerOne;
                    labelPlayerOneNumberOfWins.Text = numberOfWinsPlayerOne.ToString();
                    MessageBox.Show("Winner: " + playerOneName);
                }
                else
                {
                    ++numberOfWinsPlayerTwo;
                    labelPlayerTwoNumberOfWins.Text = numberOfWinsPlayerTwo.ToString();
                    MessageBox.Show("Winner: " + playerTwoName);
                }

                // resetira sve potrebne vrijednosti na inicijalno stanje

                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        values[i, j] = '0';
                    }
                }

                gameCount++;
                turn = true;
                turnCount = 0;
                winScenario = false;

                pictureBoxA1.Image = pictureBoxA2.Image = pictureBoxA3.Image = pictureBoxB1.Image = pictureBoxB2.Image = pictureBoxB3.Image = pictureBoxC1.Image = pictureBoxC2.Image = pictureBoxC3.Image = null;

                pbA1Clicked = pbA2Clicked = pbA3Clicked = pbB1Clicked = pbB2Clicked = pbB3Clicked = pbC1Clicked = pbC2Clicked = pbC3Clicked = false;


            }

            // u slucaju nerijesenog rezultat resetira sve potrebne vrijednosti na inicijalno stanje

            if (winScenario == false && turnCount == 9)
            {

                MessageBox.Show("It's a tie!");

                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 3; j++)
                    {
                        values[i, j] = '0';
                    }
                }

                gameCount++;
                turn = true;
                turnCount = 0;

                pictureBoxA1.Image = pictureBoxA2.Image = pictureBoxA3.Image = pictureBoxB1.Image = pictureBoxB2.Image = pictureBoxB3.Image = pictureBoxC1.Image = pictureBoxC2.Image = pictureBoxC3.Image = null;

                pbA1Clicked = pbA2Clicked = pbA3Clicked = pbB1Clicked = pbB2Clicked = pbB3Clicked = pbC1Clicked = pbC2Clicked = pbC3Clicked = false;

            }
        }


    }
}

