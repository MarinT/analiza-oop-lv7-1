﻿namespace WindowsFormsApp1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTurn = new System.Windows.Forms.Label();
            this.labelPlayerOneName = new System.Windows.Forms.Label();
            this.labelPlayerTwoName = new System.Windows.Forms.Label();
            this.labelTurnName = new System.Windows.Forms.Label();
            this.labelPlayerOneNumberOfWins = new System.Windows.Forms.Label();
            this.labelPlayerTwoNumberOfWins = new System.Windows.Forms.Label();
            this.pictureBoxA1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxA2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxA3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxB3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxB2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxB1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxC3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxC2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxC1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxA2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxA3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxC3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxC1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTurn
            // 
            this.labelTurn.AutoSize = true;
            this.labelTurn.Location = new System.Drawing.Point(15, 7);
            this.labelTurn.Name = "labelTurn";
            this.labelTurn.Size = new System.Drawing.Size(32, 13);
            this.labelTurn.TabIndex = 9;
            this.labelTurn.Text = "Turn:";
            // 
            // labelPlayerOneName
            // 
            this.labelPlayerOneName.AutoSize = true;
            this.labelPlayerOneName.Location = new System.Drawing.Point(12, 236);
            this.labelPlayerOneName.Name = "labelPlayerOneName";
            this.labelPlayerOneName.Size = new System.Drawing.Size(35, 13);
            this.labelPlayerOneName.TabIndex = 10;
            this.labelPlayerOneName.Text = "label2";
            // 
            // labelPlayerTwoName
            // 
            this.labelPlayerTwoName.AutoSize = true;
            this.labelPlayerTwoName.Location = new System.Drawing.Point(171, 236);
            this.labelPlayerTwoName.Name = "labelPlayerTwoName";
            this.labelPlayerTwoName.Size = new System.Drawing.Size(35, 13);
            this.labelPlayerTwoName.TabIndex = 11;
            this.labelPlayerTwoName.Text = "label3";
            // 
            // labelTurnName
            // 
            this.labelTurnName.AutoSize = true;
            this.labelTurnName.Location = new System.Drawing.Point(53, 7);
            this.labelTurnName.Name = "labelTurnName";
            this.labelTurnName.Size = new System.Drawing.Size(10, 13);
            this.labelTurnName.TabIndex = 12;
            this.labelTurnName.Text = "-";
            // 
            // labelPlayerOneNumberOfWins
            // 
            this.labelPlayerOneNumberOfWins.AutoSize = true;
            this.labelPlayerOneNumberOfWins.Location = new System.Drawing.Point(12, 249);
            this.labelPlayerOneNumberOfWins.Name = "labelPlayerOneNumberOfWins";
            this.labelPlayerOneNumberOfWins.Size = new System.Drawing.Size(29, 13);
            this.labelPlayerOneNumberOfWins.TabIndex = 13;
            this.labelPlayerOneNumberOfWins.Text = "label";
            // 
            // labelPlayerTwoNumberOfWins
            // 
            this.labelPlayerTwoNumberOfWins.AutoSize = true;
            this.labelPlayerTwoNumberOfWins.Location = new System.Drawing.Point(171, 249);
            this.labelPlayerTwoNumberOfWins.Name = "labelPlayerTwoNumberOfWins";
            this.labelPlayerTwoNumberOfWins.Size = new System.Drawing.Size(35, 13);
            this.labelPlayerTwoNumberOfWins.TabIndex = 14;
            this.labelPlayerTwoNumberOfWins.Text = "label3";
            // 
            // pictureBoxA1
            // 
            this.pictureBoxA1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBoxA1.Location = new System.Drawing.Point(12, 23);
            this.pictureBoxA1.Name = "pictureBoxA1";
            this.pictureBoxA1.Size = new System.Drawing.Size(72, 61);
            this.pictureBoxA1.TabIndex = 15;
            this.pictureBoxA1.TabStop = false;
            this.pictureBoxA1.Click += new System.EventHandler(this.pictureBoxA1_Click);
            // 
            // pictureBoxA2
            // 
            this.pictureBoxA2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBoxA2.Location = new System.Drawing.Point(90, 23);
            this.pictureBoxA2.Name = "pictureBoxA2";
            this.pictureBoxA2.Size = new System.Drawing.Size(72, 61);
            this.pictureBoxA2.TabIndex = 16;
            this.pictureBoxA2.TabStop = false;
            this.pictureBoxA2.Click += new System.EventHandler(this.pictureBoxA2_Click);
            // 
            // pictureBoxA3
            // 
            this.pictureBoxA3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBoxA3.Location = new System.Drawing.Point(168, 23);
            this.pictureBoxA3.Name = "pictureBoxA3";
            this.pictureBoxA3.Size = new System.Drawing.Size(72, 61);
            this.pictureBoxA3.TabIndex = 17;
            this.pictureBoxA3.TabStop = false;
            this.pictureBoxA3.Click += new System.EventHandler(this.pictureBoxA3_Click);
            // 
            // pictureBoxB3
            // 
            this.pictureBoxB3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBoxB3.Location = new System.Drawing.Point(168, 90);
            this.pictureBoxB3.Name = "pictureBoxB3";
            this.pictureBoxB3.Size = new System.Drawing.Size(72, 61);
            this.pictureBoxB3.TabIndex = 20;
            this.pictureBoxB3.TabStop = false;
            this.pictureBoxB3.Click += new System.EventHandler(this.pictureBoxB3_Click);
            // 
            // pictureBoxB2
            // 
            this.pictureBoxB2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBoxB2.Location = new System.Drawing.Point(90, 90);
            this.pictureBoxB2.Name = "pictureBoxB2";
            this.pictureBoxB2.Size = new System.Drawing.Size(72, 61);
            this.pictureBoxB2.TabIndex = 19;
            this.pictureBoxB2.TabStop = false;
            this.pictureBoxB2.Click += new System.EventHandler(this.pictureBoxB2_Click);
            // 
            // pictureBoxB1
            // 
            this.pictureBoxB1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBoxB1.Location = new System.Drawing.Point(12, 90);
            this.pictureBoxB1.Name = "pictureBoxB1";
            this.pictureBoxB1.Size = new System.Drawing.Size(72, 61);
            this.pictureBoxB1.TabIndex = 18;
            this.pictureBoxB1.TabStop = false;
            this.pictureBoxB1.Click += new System.EventHandler(this.pictureBoxB1_Click);
            // 
            // pictureBoxC3
            // 
            this.pictureBoxC3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBoxC3.Location = new System.Drawing.Point(168, 157);
            this.pictureBoxC3.Name = "pictureBoxC3";
            this.pictureBoxC3.Size = new System.Drawing.Size(72, 61);
            this.pictureBoxC3.TabIndex = 23;
            this.pictureBoxC3.TabStop = false;
            this.pictureBoxC3.Click += new System.EventHandler(this.pictureBoxC3_Click);
            // 
            // pictureBoxC2
            // 
            this.pictureBoxC2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBoxC2.Location = new System.Drawing.Point(90, 157);
            this.pictureBoxC2.Name = "pictureBoxC2";
            this.pictureBoxC2.Size = new System.Drawing.Size(72, 61);
            this.pictureBoxC2.TabIndex = 22;
            this.pictureBoxC2.TabStop = false;
            this.pictureBoxC2.Click += new System.EventHandler(this.pictureBoxC2_Click);
            // 
            // pictureBoxC1
            // 
            this.pictureBoxC1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBoxC1.Location = new System.Drawing.Point(12, 157);
            this.pictureBoxC1.Name = "pictureBoxC1";
            this.pictureBoxC1.Size = new System.Drawing.Size(72, 61);
            this.pictureBoxC1.TabIndex = 21;
            this.pictureBoxC1.TabStop = false;
            this.pictureBoxC1.Click += new System.EventHandler(this.pictureBoxC1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 272);
            this.Controls.Add(this.pictureBoxC3);
            this.Controls.Add(this.pictureBoxC2);
            this.Controls.Add(this.pictureBoxC1);
            this.Controls.Add(this.pictureBoxB3);
            this.Controls.Add(this.pictureBoxB2);
            this.Controls.Add(this.pictureBoxB1);
            this.Controls.Add(this.pictureBoxA3);
            this.Controls.Add(this.pictureBoxA2);
            this.Controls.Add(this.pictureBoxA1);
            this.Controls.Add(this.labelPlayerTwoNumberOfWins);
            this.Controls.Add(this.labelPlayerOneNumberOfWins);
            this.Controls.Add(this.labelTurnName);
            this.Controls.Add(this.labelPlayerTwoName);
            this.Controls.Add(this.labelPlayerOneName);
            this.Controls.Add(this.labelTurn);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxA2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxA3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxC3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxC1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelTurn;
        private System.Windows.Forms.Label labelPlayerOneName;
        private System.Windows.Forms.Label labelPlayerTwoName;
        private System.Windows.Forms.Label labelTurnName;
        private System.Windows.Forms.Label labelPlayerOneNumberOfWins;
        private System.Windows.Forms.Label labelPlayerTwoNumberOfWins;
        private System.Windows.Forms.PictureBox pictureBoxA1;
        private System.Windows.Forms.PictureBox pictureBoxA2;
        private System.Windows.Forms.PictureBox pictureBoxA3;
        private System.Windows.Forms.PictureBox pictureBoxB3;
        private System.Windows.Forms.PictureBox pictureBoxB2;
        private System.Windows.Forms.PictureBox pictureBoxB1;
        private System.Windows.Forms.PictureBox pictureBoxC3;
        private System.Windows.Forms.PictureBox pictureBoxC2;
        private System.Windows.Forms.PictureBox pictureBoxC1;
    }
}