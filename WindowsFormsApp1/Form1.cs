﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace WindowsFormsApp1
{

    //Uvodni windows form koji sluzi kao menu


    public partial class Form1 : Form
    {
        string playerOneName = "", playerTwoName = "";
        Thread th;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            //zatvaranje ove windows forme i slanje argumenta metodi koja ce otvoriti novu windows formu ukoliko su imena igraca unesena

            if (textBoxPlayerOne.Text != "" && textBoxPlayerTwo.Text != "")
            {
                playerOneName = textBoxPlayerOne.Text;
                playerTwoName = textBoxPlayerTwo.Text;

                this.Close();
                th = new Thread(openNewForm);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();
            }
        }

        //metoda koja otvara novu windows formu

        private void openNewForm(object obj)
        {
            Application.Run(new Form2(playerOneName, playerTwoName));
        }
    }
}
